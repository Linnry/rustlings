// macros4.rs
// Make me compile! Execute `rustlings hint macros4` for hints :)

// I AM NOT DONE

macro_rules! my_macro {
    () => {
        println!("Check out my macro!");
    };
    ($val:expr) => {
        if $val > 5000 {
        println!("Look at this other macro: {} (Quite Big)", $val);
        }
        else {
            println!("Look at this other macro: {} (Kinda small)", $val);
        }
    }
}

fn main() {
    my_macro!();
    my_macro!(7777);
    my_macro!(50);
    my_macro!(1);

}
